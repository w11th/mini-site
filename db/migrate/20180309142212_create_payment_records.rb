class CreatePaymentRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_records do |t|
      t.belongs_to :user
      t.string :item_name
      t.integer :price

      t.timestamps
    end
  end
end
