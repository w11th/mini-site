Rails.application.routes.draw do
  root to: 'payment_records#index'

  with_options controller: :sessions do
    get :signin, action: :new
    post :signin, action: :create
    delete :signout, action: :destroy
    get :signout, action: :destroy
  end

  resources :payment_records, only: [:index, :create] do
    collection do
      post :pdf
    end
  end
end
