class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  helper_method :current_user

  private

  def current_user
    return @current_user if @current_user.present?

    if session[:user_id].present?
      @current_user = User.find_by(id: session[:user_id])
    end

    @current_user
  end

  def authenticate_user!
    redirect_to signin_path if current_user.blank?
  end

  def signin(user)
    session[:user_id] = user.id
  end

  def signout
    session.delete(:user_id)
  end
end
