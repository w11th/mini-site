class PaymentRecordsController < ApplicationController
  def index
    @payment_records = current_user.payment_records.sorted
    @total_dollar_price = current_user.payment_records.total_dollar_price
  end

  def create
    @payment_record = current_user.payment_records.create(payment_record_params)
    @total_dollar_price = current_user.payment_records.total_dollar_price
  end

  private

  def payment_record_params
    params.require(:payment_record).permit(:item_name, :dollar_price)
  end
end
