class SessionsController < ApplicationController
  skip_before_action :authenticate_user!

  def new
  end

  def create
    user = User.find_or_create_by(name: params[:user_name])

    signin(user)
    redirect_to root_path
  end

  def destroy
    signout
    redirect_to signin_path
  end
end
