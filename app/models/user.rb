class User < ApplicationRecord
  has_many :payment_records, dependent: :destroy do
    def total_dollar_price
      total_price = sum(:price)
      total_price / 100.0
    end
  end

  validates :name,
            presence: true,
            uniqueness: true
end
