class PaymentRecord < ApplicationRecord
  belongs_to :user

  validates :item_name, presence: true
  validates :price,
            presence: true,
            numericality: { only_integer: true, allow_blank: true }

  scope :sorted, -> { order(id: :asc) }

  def dollar_price
    price / 100.0
  end

  def dollar_price=(dollar)
    return if dollar.blank?
    self.price = (dollar.to_f * 100).to_i
  end
end
